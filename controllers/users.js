const express = require('express');
const router = express.Router();
const User = require('../models/User')
const axiosGoRest = require('../utils/axios-go-rest')

router.get('/fetch-from-go-rest', async (req, res, next) => {
    try {
      const users = (await axiosGoRest.get('/users')).data
      await User.remove()
      await User.insertMany(users)
      res.json({message: "DB Populated Successfully", users});
      
    }catch(error){
      res.status(210).json({error: error.message});
      console.log(error)
    }
})

router.get('',async (req, res, next) => {
    try {
      const users = await User.find()
  
      res.json({message: "Users fetched", users});
      
    }catch(error){
      res.status(210).json({error: error.message});
      console.log(error)
    }
})

router.get('/:id',async (req, res, next) => {
    try {
      const user = await User.findOne({id: req.params.id})
  
      res.json({message: "User fetched", user});
      
    }catch(error){
      res.status(210).json({error: error.message});
      console.log(error)
    }
})

router.put('/:id',async (req, res, next) => {
    try {
        if(req.body.email && (await User.findOne({email: req.body.email}))){
            res.json({message: "Email already exists"});
        }
      let user = await User.findOneAndUpdate({id: req.params.id}, req.body)
      user = await User.findOne()
      res.json({message: "User updated", user});
      
    }catch(error){
      res.status(210).json({error: error.message});
      console.log(error)
    }
})

module.exports = router;
