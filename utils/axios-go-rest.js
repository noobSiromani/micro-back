require('dotenv').config()

const axios = require('axios');

const axiosInstance = axios.create({
    baseURL: process.env.GO_REST_BASE_URL,
    headers: {
        'Authorization': `Bearer ${process.env.GO_REST_TOKEN}`
    }
})

module.exports = axiosInstance